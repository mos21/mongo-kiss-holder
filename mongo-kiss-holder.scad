include <peglib/defaults.scad>;
include <moslib/libchamfer.scad>;
include <peglib/rail.scad>;

$fn = 90;

color("skyblue") 
rail(2, chamfer = wall_thickness / 4, back_wall_gap = 0.2, side_wall_gap = 0.2, copies = 1);

gap = 1;
id = 19.75 + gap;
od = id + (wall_thickness * 2);


translate([0, 13.5, 0])
closed_chamfered_tube(height = base_size * 1.5, od = od, id = id, align = [0, 0], chamfer = wall_thickness / 4, cutout = false);
